﻿using System;
using System.Collections.Generic;

namespace DomainModel
{
    interface IAddEditDelete
    {
        void Add();
        void Edit();
        void Delete();
    }

    public class Product : IAddEditDelete
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Weight { get; set; }
        public decimal Price { get; set; }

        public void Add()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Edit()
        {
            throw new NotImplementedException();
        }
    }

    public class Meal : IAddEditDelete
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ICollection<Product> Consists { get; set; }
        public decimal Weight { get; set; }
        public decimal Price { get; set; }

        public void Add()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Edit()
        {
            throw new NotImplementedException();
        }
    }

    public class Category : IAddEditDelete
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ICollection<Meal> Meals { get; set; }

        public void Add()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Edit()
        {
            throw new NotImplementedException();
        }
    }

    public class Menu : IAddEditDelete
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ICollection<Meal> Meals { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public void Add()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Edit()
        {
            throw new NotImplementedException();
        }
    }

    public class MealSet : IAddEditDelete
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ICollection<Meal> Meals { get; set; }

        public void Add()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Edit()
        {
            throw new NotImplementedException();
        }
    }

    public class Recipient : IAddEditDelete
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public void Add()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Edit()
        {
            throw new NotImplementedException();
        }
    }

    public class Order : IAddEditDelete
    {
        public string Id { get; set; }
        public string Recipient { get; set; }
        public ICollection<Meal> Meals { get; set; }
        public MealSet Set { get; set; }
        public DateTime DeliveryTime { get; set; }
        public string Location { get; set; }
        public string OtherWishes { get; set; }

        public void Add()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public void Edit()
        {
            throw new NotImplementedException();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

        }
    }
}
